
var movement = false;

var PIRpin = D14;

setWatch(function() {
  if (movement === false) {
    movement = true;
    console.log("Movement detected");
  }
}, PIRpin, {repeat:true, debounce:10, edge:"rising"});

setWatch(function() {
  if (movement === true) {
    movement = false;
    console.log("Movement end");
  }
}, PIRpin, {repeat:true, debounce:10, edge:"falling"});
