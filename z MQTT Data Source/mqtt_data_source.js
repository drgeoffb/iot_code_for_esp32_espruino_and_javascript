// global variables/objects
var dht;
var g;
var mqtt;
var wifi;

var DHT_PIN = D25;

// DHT22 temperature + humidity sensor ////////////////////////////////////
function initDHT() {
    console.log("Initialise DHT22 temperature + humidity sensor...");
    dht = require("DHT22").connect(DHT_PIN);
}

function readDHT() {
    dht.read(function(response) {
        var topic = "ict111/data/temperature";

        if (response.err === true) {
            console.log("Error retrieving data from DHT22");
            console.log(response);
            return;
        }

        var msg;
        msg.rh = response.rh;
        msg.temp = response.temp;
        var message = JSON.stringify(msg);
        publish_to_mqtt(topic, message);
    })
}

// oled display ///////////////////////////////////////////////////////////
// init function
function initDisplay(callback) {
    console.log("Initialise Display...");
    // display
    var SDA = D5;
    var SCL = D4;
    I2C1.setup({ scl: SCL, sda: SDA });
    g = require("SSD1306").connect(I2C1);
    require("Font8x16").add(Graphics);
    print_ip("Starting...");
    callback();
}

// function to print ip address at bottom of screen
function print_ip(text) {
    g.clear();
    g.setFont8x16();
    g.drawString(text, 0, 40);
    g.flip();
}
///////////////////////////////////////////////////////////////////////////

// WiFi ///////////////////////////////////////////////////////////////////
var WIFI_NAME = "WIN-WiFi";
var WIFI_PASSWORD = "wifiwin368";
WIFI_NAME = "GoodLuck88";
WIFI_PASSWORD = "c649m9ry9ft3yffhdvp667d9m";
var WIFI_OPTIONS = {
    password: WIFI_PASSWORD
};

// init function
function initWifi() {
    console.log("Initialise WiFi...");
    wifi = require("Wifi");

    wifi.stopAP();

    wifi.connect(WIFI_NAME, WIFI_OPTIONS, function(err) {
        if (err) {
            console.log("Connection error: " + err);
            return;
        }
        console.log("WiFi connected!");
    });

    wifi.on('connected', function(details) {
        console.log("WiFi is connected.");
        console.log("Details:");
        console.log(details);
        console.log("getIP():");
        console.log(wifi.getIP());
        print_ip(wifi.getIP().ip);

        // initialise MQTT
        initMQTT();
    });
}

// MQTT ///////////////////////////////////////////////////////////////////
var MQTT_BROKER = "broker.shiftr.io"; // enter server for mqtt
var MQTT_KEY = "esp32_datasource"; // enter username for mqtt
var MQTT_SECRET = "datasource"; // enter password for mqtt
var MQTT_OPTIONS = {
    username: MQTT_KEY,
    password: MQTT_SECRET
};
var mqtt_is_connected = false;

// init function
function initMQTT() {
    console.log("Initialise MQTT...");
    mqtt = require("tinyMQTT").create(MQTT_BROKER, MQTT_OPTIONS);

    mqtt.on('connected', function() {
        console.log("MQTT connected");
        mqtt_is_connected = true;
    });
}

function publish_to_mqtt(topic, message) {
    // running mqtt.connect() will reconnect if the connection is borken
    mqtt.connect();
    mqtt.publish(topic, message);
    console.log("MQTT pub: " + topic + " : " + message);
}
///////////////////////////////////////////////////////////////////////////

function startIntervals() {
    setTimeout(function() {
        setInterval(function() {
            readDHT();
        }, 10000);
    }, 1000);

    setTimeout(function() {
        setInterval(function() {
            var topic = "ict111/data/random";
            var value = ("000000" + parseInt(Math.random() * 1000000)).substr(-6);
            var message = "{\"six_digits\": " + value + "}";
            publish_to_mqtt(topic, message);
        }, 10000);
    }, 3000);
}

// startup/init ///////////////////////////////////////////////////////////
function onInit() {
    console.log("Hello, world!");
    console.log();
    initDisplay(function() {
        initWifi();
        initDHT();

        startIntervals();
    });
}
save();
