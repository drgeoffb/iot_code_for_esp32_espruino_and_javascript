
var button0 = D0;

var relay0 = D32;

function relayOpen(relay) {
	digitalWrite(relay, 1);
}

function relayClose(relay) {
	digitalWrite(relay, 0);
}

setWatch(function(e) { 
	console.log('button pressed');
	relayClose(relay0);
}, button0, {repeat: true, edge: 'falling', debounce: 100});

setWatch(function(e) { 
	console.log('button released');
	relayOpen(relay0);
}, button0, {repeat: true, edge: 'rising', debounce: 100});

// ensure we're starting with the relay 'open' or 'off'
relayOpen(relay0);
