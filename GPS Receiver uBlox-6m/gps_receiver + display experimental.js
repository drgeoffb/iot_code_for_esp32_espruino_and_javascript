
var RECEIVE  = D2;
var TRANSMIT = D15;

Serial2.setup(9600, {tx: TRANSMIT, rx: RECEIVE});
var gps = require("GPS").connect(Serial2, function(data) {
	console.log(data);
});



var WIFI_NAME = "GoodLuck88";
var WIFI_OPTIONS = { password : "c649m9ry9ft3yffhdvp667d9m" };

var wifi = require("Wifi");
wifi.connect(WIFI_NAME, WIFI_OPTIONS, function(err) {
  if (err) {
    console.log("Connection error: "+err);
    return;
  }
  console.log("Connected!");
});

function getPage() {
  console.log("getPage()");
  require("http").get("http://threefiddy.com/web/random.txt", function(res) {
    console.log("Response: ",res);
    res.on('data', function(d) {
      console.log("--->"+d);
      print_to_display(d);
    });
  });
}

setInterval(function() {
  getPage();
}, 10500);


function print_to_display(text){
  g.clear();
  g.setFontHaxorNarrow7x17();
  g.drawString(text,0,0);
  g.flip();
}


require("Font8x16").add(Graphics);
require("FontHaxorNarrow7x17").add(Graphics);

var SDA = D5;
var SCL = D4;

I2C1.setup({scl:SCL,
sda:SDA});

var g = require("SSD1306").connect(I2C1);


g.clear(); g.drawString(gpsData.time,40,40);g.flip();



function drawGPS(gpsData) {
	g.drawString("Time: " + gpsData.time, 0,12);
	g.drawString("Lat:  " + gpsData.lat,  0,24);
	g.drawString("Lon:  " + gpsData.lon,  0,36);
	g.drawString("Sats: " + gpsData.satellites, 0,48);
	g.flip();
}
