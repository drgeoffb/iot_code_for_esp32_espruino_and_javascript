

// this source is copied, unchanged, from 
// https://www.espruino.com/Reference#http

function postJSON(postURL, data, callback) {
  content = JSON.stringify(data);
  var options = url.parse(postURL);
  options.method = 'POST';
  options.headers = {
    "Content-Type":"application/json",
    "Content-Length":content.length
  };
  var req = require("http").request(options, function(res)  {
    var d = "";
    res.on('data', function(data) { d+= data; });
    res.on('close', function(data) { callback(d); });
  });
  req.on('error', function(e) {
    callback();
  });
  req.end(content);
}

postJSON("http://www.example.com", {test:42}, function(d) {
  console.log("Response: "+d);
});
