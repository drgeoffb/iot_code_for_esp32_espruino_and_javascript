// SPI bus configuration
var CHIP_SELECT = D5; // LOAD on MAX7219
var CLOCK = D18;      // CLK on MAX7219
var MOSI = D23;       // DOUT on MAX7219
//var MISO = D19;

// make an SPI object by loading the built-in SPI module
var spi = new SPI();

// configure the spi object
spi.setup({
	sck: CLOCK,
	mosi: MOSI
	//miso: MISO,
});

var disp = require("MAX7219").connect( spi, CHIP_SELECT);

disp.set("--HELP--");

var n = 0;
setInterval( function() {
  disp.set(n++);
}, 1000);
