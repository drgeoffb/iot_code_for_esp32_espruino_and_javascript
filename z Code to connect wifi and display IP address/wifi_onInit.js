// wifi config
var WIFI_NAME = "WIN-WiFi";
var WIFI_PASSWORD = "wifiwin368";
var WIFI_OPTIONS = {
    password: WIFI_PASSWORD
};

var wifi;
var g;

function initWifi() {
    wifi = require("Wifi");

    wifi.stopAP();

    wifi.connect(WIFI_NAME, WIFI_OPTIONS, function(err) {
        if (err) {
            console.log("Connection error: " + err);
            return;
        }
        console.log("WiFi connected!");
    });

    wifi.on('connected', function(details) {
        console.log("Details:");
        console.log(details);
        console.log("getIP():");
        console.log(wifi.getIP());
        print_ip("IP: " + wifi.getIP().ip);
    });
}

function initDisplay(callback) {
    // display
    var SDA = D5;
    var SCL = D4;
    I2C1.setup({ scl: SCL, sda: SDA });
    g = require("SSD1306").connect(I2C1);
    require("Font8x16").add(Graphics);
    callback();
}

// function to print ip address at bottom of screen
function print_ip(text) {
    g.clear();
    g.setFont8x16();
    g.drawString(text, 0, 40);
    g.flip();
}

function onInit() {
    initDisplay(function() {
        print_ip("Starting...");
        initWifi();
    });
}
save();


// var wifi = require("Wifi"); wifi.save("clear"); reset(); save();