// global variables/objects
var dht;
var mqtt;
var wifi;

var DHT_PIN = D2;
var DHT_TEMP_LOCATION = "local";

// DHT11 temperature + humidity sensor ////////////////////////////////////
function initDHT() {
    console.log("Initialise DHT11 temperature + humidity sensor...");
    global.dht = require("DHT11").connect(DHT_PIN);
}

function readDHT() {
    dht.read(function(response) {
        var topic = "ict111/data/temperature";

        if (response.err === true) {
            console.log("Error retrieving data from DHT11");
            console.log(response);
            return;
        }

        var msg;
        // remove the raw data (it's long, and largely useless)
        delete response.raw;

        // add a location (so that we can add others by other means later)
        response.loc = DHT_TEMP_LOCATION;

        // make it into JSON
        var message = JSON.stringify(response);

        // publish it to mqtt        
        publish_to_mqtt(topic, message);
    });
}

// WiFi ///////////////////////////////////////////////////////////////////
var WIFI_NAME = "WIN-WiFi";
var WIFI_PASSWORD = "wifiwin368";
//WIFI_NAME = "GoodLuck88";
//WIFI_PASSWORD = "c649m9ry9ft3yffhdvp667d9m";
var WIFI_OPTIONS = {
    password: WIFI_PASSWORD
};

// init function
function initWifi() {
    console.log("Initialise WiFi...");
    global.wifi = require("Wifi");

    wifi.stopAP();

    wifi.connect(WIFI_NAME, WIFI_OPTIONS, function(err) {
        if (err) {
            console.log("Connection error: " + err);
            return;
        }
        console.log("WiFi connected!");
    });

    wifi.on('connected', function(details) {
        console.log("WiFi is connected.");
        console.log("Details:");
        console.log(details);
        console.log("getIP():");
        console.log(wifi.getIP());

        // initialise MQTT
        initMQTT();
    });
}

// MQTT ///////////////////////////////////////////////////////////////////
var MQTT_BROKER = "broker.shiftr.io"; // enter server for mqtt
var MQTT_KEY = "esp01_dht0"; // enter username for mqtt
var MQTT_SECRET = "esp01pass"; // enter password for mqtt
var MQTT_OPTIONS = {
    username: MQTT_KEY,
    password: MQTT_SECRET
};
var mqtt_is_connected = false;

// init function
function initMQTT() {
    console.log("Initialise MQTT...");
    global.mqtt = require("tinyMQTT").create(MQTT_BROKER, MQTT_OPTIONS);

    mqtt.on('connected', function() {
        console.log("MQTT connected");
        mqtt_is_connected = true;
    });
}

function publish_to_mqtt(topic, message) {
    // running mqtt.connect() will reconnect if the connection is borken
    mqtt.connect();
    mqtt.publish(topic, message);
    console.log("MQTT pub: " + topic + " : " + message);
}
///////////////////////////////////////////////////////////////////////////

// time ///////////////////////////////////////////////////////////////////
var TZ_UTC = 0;
var TZ_OFFSET = 10;
var NTP_SERVER = "pool.ntp.org";
E.setTimeZone(TZ_UTC);
require("Wifi").setSNTP(NTP_SERVER, TZ_OFFSET);

function makeTime() {
    var topic = "ict111/data/time";
    var time_now = new Object();
    var date = new Date();
    //time_now.sec = date.getSeconds();
    time_now.sec = ("0" + date.getSeconds()).substr(-2).toString();
    //time_now.min = date.getMinutes();
    time_now.min = ("0" + date.getMinutes()).substr(-2).toString();
    //time_now.hr = date.getHours() + TZ_OFFSET;
    var hr = date.getHours();
    if (hr > 23) {
        hr = hr - 12;
    }
    time_now.hr = ("0" + hr).substr(-2).toString();

    time_now.zone = "AEST";
    time_now.epoch = parseInt(date.getTime()).toString();
    var message = JSON.stringify(time_now);

    publish_to_mqtt(topic, message);
}
///////////////////////////////////////////////////////////////////////////
function startIntervals() {
    setTimeout(function() {
        setInterval(function() {
            readDHT();
        }, 10000);
    }, 10000);

    setTimeout(function() {
        setInterval(function() {
            makeTime();
        }, 1000);
    }, 11000);

    setTimeout(function() {
        setInterval(function() {
            var topic = "ict111/data/random";
            var value = ("000000" + parseInt(Math.random() * 1000000)).substr(-6).toString();
            var message = "{\"six_digits\": " + value + "}";
            publish_to_mqtt(topic, message);
        }, 10000);
    }, 12000);
}

// startup/init ///////////////////////////////////////////////////////////
function onInit() {
    console.log("Hello, world!");
    console.log();

    // start wifi (the wifi.on('connected') event triggers mqtt later)
    setTimeout(function() {
        console.log("Triggering initWifi()");
        initWifi();
    }, 5000);

    // init DHT
    console.log("Triggering initDHT()");
    initDHT();

    // if the device loads wifi from pre-saved (wifi.save() config), it 
    // won't trigger the wifi.on('connected') event...
    //
    // so start mqtt after a while, to be sure
    setTimeout(function() {
        console.log("Triggering initMQTT()");
        initMQTT();
    }, 10000);

    console.log("Triggering data creation/collection intervals");
    startIntervals();
}

save();