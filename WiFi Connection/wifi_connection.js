// WiFi configuration
var WIFI_NAME = "wifi_name_here";
var WIFI_PASSWORD = "wifi_password_here";
var WIFI_OPTIONS = {
	password: WIFI_PASSWORD
};

// make a wifi object by loading the wifi module
var wifi = require("Wifi");

// connect to wifi
wifi.connect(WIFI_NAME, WIFI_OPTIONS, function(error) {
	if (error) {
		console.log("Wifi connection error: " + error);
		return;
	}

	// if we reach here, we're connected to wifi. 
	console.log("Wifi connected!");
	console.log("  IP Address: " + wifi.getIP().ip);
});
