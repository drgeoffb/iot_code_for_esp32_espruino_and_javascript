DHTPIN = D22;

var dht = require("DHT22").connect(DHTPIN);

var n = 0;

setInterval(function() {
    dht.read(function(response) {
        console.log(n);
        console.log("Temperature: " + response.temp);
        console.log("Humidity:    " + response.rh);
        console.log("-----------------------");
    });
    n++;
}, 5000);
