
// MQTT
var MQTT_SERVER = "broker.shiftr.io"; // enter server for mqtt
var MQTT_SUBSCRIBE_TOPIC = "ict111/data/temperature"; // enter topic for mqtt
//var MQTT_SUBSCRIBE_TOPIC = "ict111/data/time"; // enter topic for mqtt
//var MQTT_SUBSCRIBE_TOPIC = "ict111/data/random"; // enter topic for mqtt
var MQTT_USER = "ict111_subscribe"; // enter username for mqtt
var MQTT_PASS = "password"; // enter password for mqtt

var MQTT_OPTIONS = {
    username: MQTT_USER,
    password: MQTT_PASS
};

var mqtt_is_connected = false;

var mqtt = require("tinyMQTT").create(MQTT_SERVER, MQTT_OPTIONS);

setTimeout(function() {
	mqtt.connect();
}, 5000);

mqtt.on("connected", function(){
    mqtt.subscribe(MQTT_SUBSCRIBE_TOPIC);
});

mqtt.on("message", function(msg){
    console.log(msg.topic);
    console.log(msg.message);
});
