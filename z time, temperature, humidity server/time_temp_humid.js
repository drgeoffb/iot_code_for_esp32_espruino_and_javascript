// wifi config
var wifi; // for the wifi object later
var WIFI_NAME = "WIN-WiFi";
var WIFI_PASSWORD = "wifiwin368";
WIFI_NAME = "Telstra06FF";
WIFI_PASSWORD = "2052415521";
var WIFI_OPTIONS = {
    password: WIFI_PASSWORD
};

// graphics config
var g;

// dht (temp/humid) config
var DHTPIN = D22;
var dht;

// network time config
var NTP_SERVER = "pool.ntp.org";
var NTP_TZ = "10";

// date 
var date;

// initialisation functions

function initWifi(callback) {
    wifi = require("Wifi");

    wifi.stopAP();

    wifi.connect(WIFI_NAME, WIFI_OPTIONS, function(err) {
        if (err) {
            console.log("Connection error: " + err);
            return;
        }
        console.log("WiFi connected!");
    });

    wifi.on('connected', function(details) {
        console.log("Details:");
        console.log(details);
        console.log("getIP():");
        console.log(wifi.getIP());
        print_ip(wifi.getIP().ip);
        console.log("calling onWifi()");
        onWifi();
    });
    if (typeof callback != "undefined") { callback(); }
}

function initDisplay(callback) {
    // display
    var SDA = D5;
    var SCL = D4;
    I2C1.setup({ scl: SCL, sda: SDA });
    g = require("SSD1306").connect(I2C1);
    require("Font8x16").add(Graphics);
    console.log("initDisplay() is done");
    if (typeof callback != "undefined") { callback(); }
}

function initDHT(callback) {
    dht = require("DHT22").connect(DHTPIN);
    console.log("initDHT() is done");
    if (typeof callback != "undefined") { callback(); }
}

function initNTP(callback) {
    wifi.setSNTP(NTP_SERVER, NTP_TZ);
    console.log("initNTP() is done");
    if (typeof callback != "undefined") { callback(); }
}

function initDate(callback) {
    date = Date();
    E.setTimeZone(parseInt(NTP_TZ));
    console.log("initDate() is done");
    if (typeof callback != "undefined") { callback(); }
}

// function to print ip address at bottom of screen
function print_ip(text, callback) {
    g.clear();
    g.setFont8x16();
    g.drawString(text, 0, 40);
    g.flip();
    if (typeof callback != "undefined") { callback(); }
}

// function to return present time as a strong
function getTimeNow(callback) {
    var timeNow = date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
    if (typeof callback != "undefined") { callback(timeNow); }
}

// function to print a bunch of things on the oled display
function print_things(time, temp, humid, ip, count, callback) {
    g.clear();
    g.setFont8x16();
    g.drawString("Time:  " + time, 0, 0);
    g.drawString("Temp:  " + temp, 0, 12);
    g.drawString("Humid: " + humid, 0, 24);
    g.drawString("IP:    " + ip, 0, 36);
    g.drawString("n:     " + count, 0, 48);
    g.flip();
    if (typeof callback != "undefined") { callback(); }
}

function onWifi(callback) {
    // things that should be initialised after wifi is up
    initNTP();
    initDate();
    if (typeof callback != "undefined") { callback(); }
}

function onInit() {
    initDisplay(function() {
        initWifi();
        initDHT(); // dht22 temperature/humidity sensor
    });
}
save();




// var wifi = require("Wifi"); wifi.save("clear"); reset(); save();