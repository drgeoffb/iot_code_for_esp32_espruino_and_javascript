
var button0 = D0;

setWatch(function(e) { 
	console.log('button pressed');
}, button0, {repeat: true, edge: 'falling', debounce: 100});

setWatch(function(e) { 
	console.log('button released');
}, button0, {repeat: true, edge: 'rising', debounce: 100});

