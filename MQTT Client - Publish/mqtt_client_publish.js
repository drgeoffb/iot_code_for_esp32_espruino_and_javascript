// NOTE: MQTT requires wifi. make sure you add code to connect 
// to wifi that runs before mqtt.connect() is triggered

// MQTT
var MQTT_SERVER = ""; // enter server for mqtt
var MQTT_PUBLISH_TOPIC = ""; // enter topic for mqtt
var MQTT_USER = ""; // enter username for mqtt
var MQTT_PASS = ""; // enter password for mqtt

// ### temp
MQTT_SERV = "broker.shiftr.io";
MQTT_PUBLISH_TOPIC = "energy_monitor_mono/random_no";
MQTT_USER = "cc311c56";
MQTT_PASS = "c48d81cb1451f584";
// ###

var MQTT_OPTIONS = {
    username: MQTT_USER,
    password: MQTT_PASS
};

var mqtt_is_connected = false;

var mqtt = require("tinyMQTT").create(MQTT_SERV, MQTT_OPTIONS);

mqtt.on('connected', function() {
	mqtt_is_connected = true;
    console.log("MQTT connected");
});

mqtt.on('publish', function(pub) {
    console.log("topic: " + pub.topic);
    console.log("message: " + pub.message);
});

function publish_to_mqtt(topic, message) {
	if (!mqtt_is_connected) {
		mqtt.connect();
	}
	mqtt.publish(topic, message);
}
